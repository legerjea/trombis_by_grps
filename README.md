
# Outils pour générer des trombinoscope par groupes à partir des groupes sur moodle

## Installation

### Moyen recommandé `pipx`

`pipx` install la commande avec les dépendances dans un environnement isolé.

```
pipx install trombis-by-grps --index-url https://gitlab.utc.fr/api/v4/projects/14695/packages/pypi/simple
```

### Autre moyen

L'installer avec `pip` directement.

```
pip install trombis-by-grps --index-url https://gitlab.utc.fr/api/v4/projects/14695/packages/pypi/simple
```

Attention: il est souvent une bonne idée de créer un venv. Si vous ne savez pas
comment faire, vous deveriez utiliser le moyen recommandé d'installation.

# Utilisation

1. Récupérer sur moodle la liste des participants avec les groupes. (Se
   connecter sur le cours, aller dans _Participants_, puis en bas, cliquer sur
   _Sélectionner les xxx utilisateurs_, puis télécharger au format `.csv`)

2. Récupérer le trombi de l'UV complète. (Se connecter sur l'application _Liste
   des étudiants inscrits à une UV_, puis sélectionner le semestre et l'UV,
   attendre le chargement complet, puis clic-droit, _Enregistrer sous_. Le
   format doit être une page HTML complète).

3. Lancer la commande:

   ```
   trombi_by_grps FICHIER_FROM_MOODLE.csv TROMBI.html sortie/
   ```

   Avec:

     - `FICHIER_FROM_MOODLE.csv` le fichier qui vient de moodle à l'étape 1.
     - `TROMBI.html` le fichier qui vient du trombi enregistré à l'étape 2.
     - `sortie/` le répertoire de sortie.

   Bonus:

     - Changer le titre de chaque page: par exemple, pour mettre comme entête
       `Hiver 1931 -- YZ92 -- {group}` avec `{group}` le numéro du groupe:

     
       ```
       trombi_by_grps -H "Hiver 1931 -- YZ92 -- {group}" FICHIER_FROM_MOODLE.csv TROMBI.html sortie/
       ```

     - on peut changer le nombre de photos par page, par exemple pour mettre 4
       photos par ligne et 4 photos par colonne:

       ```
       trombi_by_grps -r 4 -c 4 FICHIER_FROM_MOODLE.csv TROMBI.html sortie/
       ```

     - on peut changer la taille de base d'écriture:
       
       ```
       trombi_by_grps -f 12 FICHIER_FROM_MOODLE.csv TROMBI.html sortie/
       ```

