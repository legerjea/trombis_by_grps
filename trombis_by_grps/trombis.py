import typing
import re
import base64
import csv
import io
import pathlib
import tempfile
import itertools
import subprocess as sp
import shutil
import sys

import more_itertools
import bs4

RE_IMG = re.compile(r"data:\s*image/jpe?g;\s*base64,\s*(\S.*)")
RE_MAIL = re.compile(r"mailto:\s*(\S.*)")


class EtudiantFromTrombi(typing.NamedTuple):
    mail: str
    photo: bytes | None

    @classmethod
    def from_table_row(cls, row: bs4.element.Tag):
        imgs = row.find_all("img")
        content_img = None
        if imgs:
            img = imgs[0]
            if match := RE_IMG.match(img.attrs["src"]):
                content_img = base64.decodebytes(match.groups()[0].encode("utf8"))
        if not (match := RE_MAIL.match(row.find_all("a")[0].attrs["href"])):
            return None
        (mail,) = match.groups()
        return cls(mail=mail, photo=content_img)


class EtudiantFromMoodle(typing.NamedTuple):
    nom: str
    prenom: str
    mail: str
    groups: tuple[str]

    @classmethod
    def from_csv_row(cls, row: dict):
        return cls(
            nom=row["Nom de famille"],
            prenom=row["Prénom"],
            mail=row["Adresse de courriel"],
            groups=tuple(x for x in row["Groupes"].replace(" ", "").split(",") if x),
        )


def get_etus_from_trombi(filedesc):
    content = bs4.BeautifulSoup(filedesc.read(), features="lxml")
    tables = content.find_all("table")
    if not tables:
        return []
    table = tables[0]
    tbodys = table.find_all("tbody")
    if not tbodys:
        return []
    return [EtudiantFromTrombi.from_table_row(tr) for tr in tbodys[0].find_all("tr")]


def get_etus_from_moodle(filedesc):
    content = filedesc.read()
    content = content.replace("\ufeff", "")
    with io.StringIO(content) as f:
        return [
            EtudiantFromMoodle.from_csv_row(row)
            for row in csv.DictReader(f, dialect=csv.excel)
        ]


def generate_trombi(
    group: str,
    list_from_moodle: list[EtudiantFromMoodle],
    list_from_trombi: list[EtudiantFromTrombi],
    output_file: pathlib.Path,
    rows: int = 5,
    cols: int = 5,
    header: str | None = None,
    fontsize: int | None = None,
):
    if header is None:
        header = "{group}"
    if fontsize is None:
        fontsize = 10
    trombi = {etu.mail: etu for etu in list_from_trombi}
    etus = [etu for etu in list_from_moodle if group in etu.groups]
    etus_pos = [
        (i // (rows * cols), (i // cols) % rows, i % cols, etu)
        for i, etu in enumerate(etus)
    ]
    with tempfile.TemporaryDirectory() as tmpdir:
        tmpdir = pathlib.Path(tmpdir)
        (tmpdir / "imgs").mkdir(parents=True, exist_ok=True)
        with open(tmpdir / f"{group}.tex", "w", encoding="utf8") as f:
            print(r"\documentclass[a4paper," + f"{fontsize}" + "pt]{article}", file=f)
            print(r"\usepackage[hmargin=1cm,vmargin=1cm]{geometry}", file=f)
            print(r"\usepackage{fontspec}", file=f)
            print(r"\usepackage{libertinus}", file=f)
            print(r"\usepackage{graphicx}", file=f)
            print(r"\pagestyle{empty}", file=f)
            print(r"\begin{document}", file=f)

            for _, page_etus in itertools.groupby(etus_pos, key=lambda x: x[0]):
                print(r"\begin{center}", file=f)
                if header:
                    print(r"{\large " + header.format(group=group) + "}", file=f)
                    print(r"", file=f)
                print(r"\vspace{.5cm}", file=f)
                print(r"\begin{tabular}{" + "c" * cols + "}", file=f)

                for _, row_etus in itertools.groupby(page_etus, key=lambda x: x[1]):
                    row_etus = list(row_etus)
                    for _, last, (p, r, c, etu) in more_itertools.mark_ends(row_etus):
                        etu_trombi = trombi.get(etu.mail)
                        if etu_trombi is not None and etu_trombi.photo is not None:
                            with open(tmpdir / "imgs" / f"{p}_{r}_{c}.jpg", "wb") as fi:
                                fi.write(etu_trombi.photo)
                            print(
                                r"\includegraphics[width="
                                + f"{1/cols/1.3:.3f}"
                                + r"\textwidth]{imgs/"
                                + f"{p}_{r}_{c}.jpg"
                                + "}",
                                file=f,
                            )
                        if not last:
                            print("&", file=f)
                    print(r"\\", file=f)
                    for _, last, (p, r, c, etu) in more_itertools.mark_ends(row_etus):
                        print(
                            r"\begin{minipage}{" + f"{1/cols/1.3:.3f}" + r"\textwidth}",
                            file=f,
                        )
                        print(f"{etu.prenom}" + r"\\" + f"{etu.nom}", file=f)
                        print(r"\end{minipage}", file=f)
                        if not last:
                            print("&", file=f)
                    print(r"\\", file=f)
                    print(r"\\", file=f)
                print(r"\end{tabular}", file=f)
                print(r"\end{center}", file=f)
                print(r"\clearpage", file=f)
            print(r"\end{document}", file=f)

        ret = sp.run(
            ("xelatex", f"{group}.tex"), cwd=tmpdir, capture_output=True, check=False
        )
        if ret.returncode == 0:
            shutil.copy(tmpdir / f"{group}.pdf", output_file)
        else:
            print(f"error: dir to examine the error: {tmpdir}")
            _ = input("enter anything to continue")


def generate_all_trombis(
    list_from_moodle: list[EtudiantFromMoodle],
    list_from_trombi: list[EtudiantFromTrombi],
    output_dir: pathlib.Path,
    rows: int = 5,
    cols: int = 5,
    verbose: bool = False,
    header: str | None = None,
    fontsize: int | None = None,
):
    groups = set(itertools.chain.from_iterable(etu.groups for etu in list_from_moodle))
    output_dir = pathlib.Path(output_dir)
    output_dir.mkdir(parents=True, exist_ok=True)
    for group in groups:
        file = output_dir / f"{group}.pdf"
        generate_trombi(
            group,
            list_from_moodle,
            list_from_trombi,
            file,
            rows,
            cols,
            header,
            fontsize,
        )
        if verbose:
            print(file, file=sys.stderr)
