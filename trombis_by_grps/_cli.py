import argparse

from . import trombis


def _posint(x):
    x = int(x)
    if x <= 0:
        raise ValueError
    return x


class file_endswith:
    def __init__(self, end):
        self._end = end

    def __call__(self, filename):
        if not filename.endswith("." + self._end):
            raise ValueError
        return argparse.FileType("r")(filename)


def arg_parser():
    parser = argparse.ArgumentParser(prog="trombis_by_grps")

    parser.add_argument("-r", "--rows", help="number of rows", type=_posint, default=5)
    parser.add_argument(
        "-c", "--columns", help="number of columns", type=_posint, default=5
    )
    parser.add_argument(
        "-q", "--quiet", help="quiet mode", dest="verbose", action="store_false"
    )
    parser.add_argument(
        "-H",
        "--header",
        help='header. E.g. "UVname -- {group}".',
        type=str,
        default=None,
    )
    parser.add_argument(
        "-f", "--fontsize", help="Fontsize", choices=("10", "11", "12"), default=None
    )
    parser.add_argument(
        "csv_from_moodle", help="csv file from moodle", type=file_endswith("csv")
    )
    parser.add_argument(
        "trombi_html_from_ent",
        help="trombi in html from ent",
        type=file_endswith("html"),
    )
    parser.add_argument("output_dir", help="output directory")

    return parser


def main():
    args = arg_parser().parse_args()

    list_from_moodle = trombis.get_etus_from_moodle(args.csv_from_moodle)
    list_from_trombi = trombis.get_etus_from_trombi(args.trombi_html_from_ent)
    trombis.generate_all_trombis(
        list_from_moodle,
        list_from_trombi,
        args.output_dir,
        rows=args.rows,
        cols=args.columns,
        verbose=args.verbose,
        header=args.header,
        fontsize=args.fontsize,
    )
